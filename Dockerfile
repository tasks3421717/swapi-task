FROM node:16

RUN mkdir -p /app/build
WORKDIR /app

COPY package.json ./
COPY yarn.lock ./

RUN yarn cache clean

RUN yarn install
COPY . ./

EXPOSE 3000
CMD ["yarn", "start"]
