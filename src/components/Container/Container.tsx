import { FC, PropsWithChildren } from 'react';
import './Container.scss';
import classNames from 'clsx';

type ContainerProps = {
  className?: string;
  variant?: 'main' | 'footer' | 'header' | 'div';
};
const Container: FC<PropsWithChildren<ContainerProps>> = ({ children, className, variant = 'div' }) => {
  const Component = variant;

  const classes = classNames('container', {
    [`${className}`]: !!className
  });
  return <Component className={classes}>{children}</Component>;
};

export default Container;
