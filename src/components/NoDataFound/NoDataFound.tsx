import SearchEmpty from './assets/SearchEmpty';
import { FC } from 'react';
import './NoDataFound.scss';

const NoDataFound: FC = () => {
  return (
    <div className="no-data-found">
      <div className="search-icon">
        <SearchEmpty />
      </div>
      <div className="no-data-found__info">
        <div>
          <span className="result">No results found</span>
        </div>
        <div>
          <span className="advice">Try to add another search combination</span>
        </div>
      </div>
    </div>
  );
};

export default NoDataFound;
