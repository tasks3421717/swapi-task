import { fireEvent, screen } from '@testing-library/react';
import Header from './Header';
import { customRender } from 'utils/testing/customRender';

const onSearchValueChange = jest.fn();

describe('Header', () => {
  test('renders the header with the home icon and search input if on home page', () => {
    customRender(<Header searchValue="test" onSearchValueChange={onSearchValueChange} />);

    expect(screen.getByText('Home')).toBeInTheDocument();
    expect(screen.getByTestId('search-character')).toBeInTheDocument();
    expect(screen.getByTestId('search-character')).toHaveValue('test');
  });

  test('does not render the search input if not on home page', () => {
    customRender(<Header />);

    expect(screen.getByText('Home')).toBeInTheDocument();
    expect(screen.queryByTestId('search-character')).toBeNull();
  });

  test('calls onSearchValueChange when the search input value changes', () => {
    customRender(<Header searchValue="old value" onSearchValueChange={onSearchValueChange} />);

    const searchInput = screen.getByTestId('search-character');
    fireEvent.change(searchInput, { target: { value: 'new value' } });
    expect(onSearchValueChange).toHaveBeenCalledWith(expect.anything());
  });
});
