import { ChangeEvent, FC } from 'react';
import './Header.scss';
import SearchIcon from '@mui/icons-material/Search';
import HomeIcon from '@mui/icons-material/Home';
import HeaderOption from './HeaderOption/HeaderOption';
import Container from 'components/Container/Container';
import { routes } from 'utils/consts/route.consts';

type HeaderProps = {
  searchValue?: string;
  onSearchValueChange?: (event: ChangeEvent<HTMLInputElement>) => void;
};

const Header: FC<HeaderProps> = ({ searchValue, onSearchValueChange }) => {
  return (
    <Container className="header">
      <div className="header__left">
        <HeaderOption Icon={HomeIcon} title="Home" link={routes.home} />
        {onSearchValueChange && (
          <div className="header__search">
            <SearchIcon />
            <input data-testid="search-character" type="text" value={searchValue} onChange={onSearchValueChange} />
          </div>
        )}
      </div>
    </Container>
  );
};

export default Header;
