import { SvgIconTypeMap } from '@mui/material';
import { OverridableComponent } from '@mui/material/OverridableComponent';
import { FC } from 'react';
import './HeaderOption.scss';
import { Link } from 'react-router-dom';

type HeaderOptionProps = {
  Icon?: OverridableComponent<SvgIconTypeMap<Record<string, unknown>, 'svg'>> & {
    muiName: string;
  };
  title: string;
  link: string;
};

const HeaderOption: FC<HeaderOptionProps> = ({ Icon, title, link }) => {
  return (
    <Link to={link} className="header-option">
      {Icon && <Icon className="header-option__icon" />}
      <h3 className="header-option__title">{title}</h3>
    </Link>
  );
};

export default HeaderOption;
