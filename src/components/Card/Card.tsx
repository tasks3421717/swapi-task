import { FC } from 'react';
import { ICharacterDto } from 'store/models/character/dto/fetch-character.dto';
import './Card.scss';

type CardProps = {
  character: ICharacterDto;
};
const Card: FC<CardProps> = ({ character }) => {
  const { imgUrl, species, name, gender } = character;

  return (
    <div className="card">
      <div className="img-wrapper">
        <img src={imgUrl} />
      </div>
      <div>
        <div className="card-desc">
          <span className="name">{name}</span>
          <div>
            <span className="model"> {species} Species</span>
          </div>
          <div>
            <span className="gender">{gender} </span>
            <span className="gender-desc">gender</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Card;
