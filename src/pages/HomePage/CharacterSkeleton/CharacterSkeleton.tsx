import Skeleton from 'react-loading-skeleton';
import './CharacterSkeleton.scss';

const CharacterSkeleton = () => {
  return (
    <div data-testid="character-skeleton" className="characters-skeleton__wrapper">
      {Array(10)
        .fill(0)
        .map((_, key) => (
          <div key={key} className="character-skeleton">
            <div className="character-skeleton__image">
              <Skeleton />
            </div>
            <div className="character-skeleton__desc">
              <Skeleton count={3} />
            </div>
          </div>
        ))}
    </div>
  );
};

export default CharacterSkeleton;
