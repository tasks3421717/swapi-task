import '@testing-library/jest-dom';
import HomePage from './HomePage';
import { customRender } from 'utils/testing/customRender';
import { cleanup, waitForElementToBeRemoved, screen, fireEvent } from '@testing-library/react';
import { server } from 'mocks/server';
import {
  FETCH_FILTERED_CHARACTERS_RESPONSE,
  FETCH_WITH_PAGE_VALUE_FILTERED_CHARACTERS_RESPONSE
} from 'mocks/handlers/characters';

afterEach(cleanup);

describe('HomePage', () => {
  it('should render without error', async () => {
    const searchExpression = '';
    const page = 1;
    server.use(...FETCH_FILTERED_CHARACTERS_RESPONSE(searchExpression, page));
    customRender(<HomePage />);
  });

  it('should show all data', async () => {
    const searchExpression = '';
    const page = 1;
    server.use(...FETCH_FILTERED_CHARACTERS_RESPONSE(searchExpression, page));
    customRender(<HomePage />);
    await waitForElementToBeRemoved(() => screen.getByTestId('character-skeleton'));
    expect(screen.getByText('Rick Sanchez')).toBeInTheDocument();
    expect(screen.getByText('Morty Smith')).toBeInTheDocument();
  });

  it('should show only Rick Sanchez', async () => {
    const searchEcpression = '';
    const page = 1;
    server.use(...FETCH_FILTERED_CHARACTERS_RESPONSE(searchEcpression, page));
    customRender(<HomePage />);
    await waitForElementToBeRemoved(() => screen.getByTestId('character-skeleton'));
    expect(screen.getByText('Rick Sanchez')).toBeInTheDocument();
    expect(screen.getByText('Morty Smith')).toBeInTheDocument();
    const name = 'Morty Smith';
    const searchInput = screen.getByTestId('search-character');
    fireEvent.change(searchInput, { target: { value: name } });
    server.use(...FETCH_FILTERED_CHARACTERS_RESPONSE(name, page));
    await waitForElementToBeRemoved(() => screen.queryByText('Rick Sanchez'));
    await waitForElementToBeRemoved(() => screen.getByTestId('character-skeleton'));
    expect(screen.queryByText('Rick Sanchez')).not.toBeInTheDocument();
  });

  it('should show the second page data', async () => {
    const searchEcpression = '';
    let page = 1;
    server.use(...FETCH_WITH_PAGE_VALUE_FILTERED_CHARACTERS_RESPONSE(searchEcpression, page));
    customRender(<HomePage />);
    await waitForElementToBeRemoved(() => screen.getByTestId('character-skeleton'));
    expect(screen.getByText('Rick Sanchez')).toBeInTheDocument();
    expect(screen.getByText('Morty Smith')).toBeInTheDocument();
    const button = screen.getByText('2');
    fireEvent.click(button);
    page = 2;
    server.use(...FETCH_WITH_PAGE_VALUE_FILTERED_CHARACTERS_RESPONSE(searchEcpression, page));
    await waitForElementToBeRemoved(() => screen.getByTestId('character-skeleton'));
    expect(screen.queryByText('Rick Sanchez')).not.toBeInTheDocument();
    expect(screen.queryByText('Morty Smith')).not.toBeInTheDocument();

    expect(screen.getByText('Beth Sanchez')).toBeInTheDocument();
    expect(screen.getByText('Beta-Seven')).toBeInTheDocument();
  });
});
