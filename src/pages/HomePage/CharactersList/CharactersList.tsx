import { FC } from 'react';
import './CharactersList.scss';
import { ICharacterDto } from 'store/models/character/dto/fetch-character.dto';
import Card from 'components/Card/Card';
import { Link } from 'react-router-dom';
import { replacePathParams } from 'utils/helpers/route.helper';
import { routes } from 'utils/consts/route.consts';

interface CharactersListProps {
  characters: ICharacterDto[];
}
const CharactersList: FC<CharactersListProps> = ({ characters }) => {
  return (
    <div className="characters-list">
      {characters.map((character) => (
        <Link
          key={character.id}
          to={replacePathParams(routes.character, {
            id: character.id
          })}>
          <Card character={character} />
        </Link>
      ))}
    </div>
  );
};

export default CharactersList;
