import { ChangeEvent, useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from 'store/hooks/redux';
import { Pagination } from '@mui/material';
import Header from 'components/Header/Header';
import Container from 'components/Container/Container';
import NoDataFound from 'components/NoDataFound/NoDataFound';
import CharacterSkeleton from './CharacterSkeleton/CharacterSkeleton';
import CharactersList from './CharactersList/CharactersList';
import { fetchCharacters } from 'store/reducers/character/CharacterActionCreators';
import './HomePage.scss';
import useDebounce from 'utils/hooks/use-debounce';

const HomePage = () => {
  const dispatch = useAppDispatch();
  const { characters, isLoading, count } = useAppSelector((state) => state.characterReducer);
  const [searchValue, setSearchValue] = useState('');
  const [page, setPage] = useState(1);
  const charactersPerPage = 20;
  const pagesNumber = Math.ceil(count / charactersPerPage);
  const debouncedValue = useDebounce(searchValue, 500);

  useEffect(() => {
    dispatch(fetchCharacters({ page, searchValue }));
  }, [page, debouncedValue, dispatch]);

  const handlePaginationChange = (_: ChangeEvent<unknown>, value: number) => {
    setPage(value);
  };

  const handleSearchValueChange = (event: ChangeEvent<HTMLInputElement>) => {
    setPage(1);
    setSearchValue(event.target.value);
  };

  return (
    <>
      <Header searchValue={searchValue} onSearchValueChange={handleSearchValueChange} />
      {!isLoading ? (
        <Container variant="main" className="home-page__container">
          {characters.length === 0 ? <NoDataFound /> : <CharactersList characters={characters} />}
          {pagesNumber > 1 && <Pagination count={pagesNumber} page={page} onChange={handlePaginationChange} />}
        </Container>
      ) : (
        <Container variant="main">
          <CharacterSkeleton />
        </Container>
      )}
    </>
  );
};

export default HomePage;
