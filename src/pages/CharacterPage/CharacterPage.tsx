import Header from 'components/Header/Header';
import CharacterDetails from './CharacterDetails/CharacterDetails';

const CharacterPage = () => {
  return (
    <>
      <Header />
      <CharacterDetails />
    </>
  );
};

export default CharacterPage;
