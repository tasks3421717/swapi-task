import { FC } from 'react';
import { useForm } from 'react-hook-form';
import { ICharacterDto } from 'store/models/character/dto/fetch-character.dto';
import { Button } from '@mui/material';
import './EditFeatures.scss';
import InputComponent from './InputComponent/InputComponent';
import { useAppDispatch } from 'store/hooks/redux';
import { characterSlice } from 'store/reducers/character/CharacterSlice';

type EditFeaturesProps = {
  character: ICharacterDto;
  setEditMode: (editMode: boolean) => void;
};

export interface EditFeaturesData {
  id?: number;
  name: string;
  species: string;
  status: string;
  type: string;
  gender: string;
  url: string;
  created: string;
  image: string;
  origin: {
    name: string;
  };
  location: {
    name: string;
    url: string;
  };
  episode: string[];
}

const EditFeatures: FC<EditFeaturesProps> = ({ character, setEditMode }) => {
  const {
    control,
    handleSubmit,
    formState: { errors }
  } = useForm<EditFeaturesData>();

  const dispatch = useAppDispatch();

  const onSubmit = (data: EditFeaturesData) => {
    dispatch(characterSlice.actions.updateCharacter({ ...data, id: character.id }));
    setEditMode(false);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="form">
      <div className="buttons">
        <Button variant="contained" color="error" size="medium" onClick={() => setEditMode(false)}>
          Cancel
        </Button>
        <Button variant="contained" color="success" size="medium" type="submit">
          Save
        </Button>
      </div>
      <div className="inputs-wrapper">
        <div className="inputs">
          <InputComponent
            control={control}
            placeholder="Name"
            name="name"
            error={errors.name}
            defaultValue={character.name}
          />
          <InputComponent
            control={control}
            placeholder="Gender"
            name="gender"
            error={errors.gender}
            defaultValue={character.gender}
          />
          <InputComponent
            control={control}
            placeholder="Location name"
            name="location.name"
            error={errors.location?.name}
            defaultValue={character.location?.name || ''}
          />
          <InputComponent
            control={control}
            placeholder="Origin name"
            name="origin.name"
            error={errors.origin?.name}
            defaultValue={character.origin.name || ''}
          />
          <InputComponent
            control={control}
            placeholder="Species"
            name="species"
            error={errors.species}
            defaultValue={character.species}
          />
          <InputComponent
            control={control}
            placeholder="Status"
            name="status"
            error={errors.status}
            defaultValue={character.status || ''}
          />
          {character.type && (
            <InputComponent
              control={control}
              placeholder="Type"
              name="type"
              error={errors.type}
              defaultValue={character.type || ''}
            />
          )}
        </div>
      </div>
    </form>
  );
};

export default EditFeatures;
