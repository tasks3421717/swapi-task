import { fireEvent, screen } from '@testing-library/react';
import EditFeatures from './EditFeatures';
import { customRender } from 'utils/testing/customRender';
import { mockedcharacters } from 'mocks/domains/character';

describe('EditFeatures component', () => {
  const setEditMode = jest.fn();

  it('should render the form with the correct inputs and buttons', () => {
    const { getByPlaceholderText, getByText } = customRender(
      <EditFeatures character={mockedcharacters[6]} setEditMode={setEditMode} />
    );

    expect(getByPlaceholderText('Name')).toBeInTheDocument();
    expect(getByPlaceholderText('Gender')).toBeInTheDocument();
    expect(getByPlaceholderText('Location name')).toBeInTheDocument();
    expect(getByPlaceholderText('Origin name')).toBeInTheDocument();
    expect(getByPlaceholderText('Species')).toBeInTheDocument();
    expect(getByPlaceholderText('Status')).toBeInTheDocument();
    expect(getByPlaceholderText('Type')).toBeInTheDocument();

    expect(getByText('Cancel')).toBeInTheDocument();
    expect(getByText('Save')).toBeInTheDocument();
  });

  it('should call setEditMode with false when the Cancel button is clicked', () => {
    const { getByText } = customRender(<EditFeatures character={mockedcharacters[0]} setEditMode={setEditMode} />);

    fireEvent.click(getByText('Cancel'));

    expect(setEditMode).toHaveBeenCalledWith(false);
  });

  it('shoud renders cancel and save buttons', () => {
    customRender(<EditFeatures character={mockedcharacters[0]} setEditMode={jest.fn()} />);
    const cancelButton = screen.getByRole('button', { name: /cancel/i });
    const saveButton = screen.getByRole('button', { name: /save/i });
    expect(cancelButton).toBeInTheDocument();
    expect(saveButton).toBeInTheDocument();
  });
});
