import { FormHelperText, Input } from '@mui/material';
import { Control, Controller, FieldError } from 'react-hook-form';
import './InputComponent.scss';

interface InputComponentProps<T> {
  control?: T;
  error?: FieldError;
  name: string;
  defaultValue: string;
  placeholder?: string;
}

const InputComponent = <T,>({ error, placeholder, name, defaultValue, control }: InputComponentProps<T>) => {
  return (
    <div className="input-component">
      <span className="title">{placeholder}</span>
      <Controller
        control={control as Control}
        name={name}
        defaultValue={defaultValue}
        rules={{ required: 'Field is required' }}
        render={({ field }) => <Input {...field} type="text" placeholder={placeholder} />}
      />
      {error && <FormHelperText error>{error.message as string}</FormHelperText>}
    </div>
  );
};

export default InputComponent;
