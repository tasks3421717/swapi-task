import { FC } from 'react';
import { ICharacterDto } from 'store/models/character/dto/fetch-character.dto';
import './Features.scss';
import { Button } from '@mui/material';
import Feature from './Feature/Feature';

type FeaturesProps = {
  character: ICharacterDto;
  setEditMode: (editMode: boolean) => void;
};
const Features: FC<FeaturesProps> = ({ character, setEditMode }) => {
  return (
    <div className="features">
      <div className="button">
        <Button variant="contained" size="medium" onClick={() => setEditMode(true)}>
          Edit
        </Button>
      </div>

      <div className="features-desc">
        <div className="features-desc-container">
          <div className="features-desc-title">
            <span>{character.name}</span>
          </div>
          <Feature title="Gender" value={character.gender} />
          {character.origin.name && <Feature title="Location name" value={character.location.name} />}
          {character.origin.name && <Feature title="Origin name" value={character.origin.name} />}
          <Feature title="Species" value={character.species} />
          {character.status && <Feature title="Status" value={character.status} />}
          {character.type && <Feature title="Type" value={character.type} />}
        </div>
      </div>
    </div>
  );
};

export default Features;
