import { FC } from 'react';
import './Feature.scss';

type FeatureProps = {
  title: string;
  value: string;
};

const Feature: FC<FeatureProps> = ({ title, value }) => {
  return (
    <div className="feature">
      <div className="feature-title">
        <span>{title}</span>
      </div>
      <div className="feature-value">
        <span>{value}</span>
      </div>
    </div>
  );
};

export default Feature;
