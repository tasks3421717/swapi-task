import Container from 'components/Container/Container';
import Card from 'components/Card/Card';
import { useParams } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from 'store/hooks/redux';
import { useEffect, useState } from 'react';
import './CharacterDetails.scss';
import { fetchCharacter } from 'store/reducers/character/CharacterActionCreators';
import EditFeatures from './EditFeatures/EditFeatures';
import Features from './Features/Features';

const CharacterDetails = () => {
  const params = useParams();
  const { id } = params;
  const { character, isLoading } = useAppSelector((state) => state.characterReducer);
  const dispatch = useAppDispatch();
  const [editMode, setEditMode] = useState(false);

  useEffect(() => {
    dispatch(fetchCharacter({ id: Number(id) }));
  }, [dispatch]);

  if (isLoading || !character) return <span aria-label="Loading" />;

  return (
    <Container className="character-details__container">
      <div className="character-details__left">
        <div className="character-details__left__header">
          <div className="character-details__left__header-titles">
            <span className="character-details__left__header-name">{character.name}</span>
            <span className="character-details__left__header-model">{character.species}</span>
          </div>
        </div>
        <Card character={character} />
      </div>
      <div className="character-details__right">
        {editMode ? (
          <EditFeatures character={character} setEditMode={setEditMode} />
        ) : (
          <Features character={character} setEditMode={setEditMode} />
        )}
      </div>
    </Container>
  );
};

export default CharacterDetails;
