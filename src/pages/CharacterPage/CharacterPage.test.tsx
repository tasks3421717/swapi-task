import '@testing-library/jest-dom';
import CharacterPage from './CharacterPage';
import { FETCH_CHARACTER_RESPONSE } from 'mocks/handlers/characters';
import { cleanup, waitForElementToBeRemoved, screen, fireEvent, render, getAllByText } from '@testing-library/react';
import { server } from 'mocks/server';
import { MemoryRouter as Router, Routes, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from 'App';

afterEach(cleanup);

describe('characterPage', () => {
  it('should render without error', async () => {
    render(
      <Provider store={store}>
        <Router initialEntries={['/characters/1']}>
          <Routes>
            <Route path="/characters/:id" element={<CharacterPage />} />
          </Routes>
        </Router>
      </Provider>
    );

    server.use(...FETCH_CHARACTER_RESPONSE());

    await waitForElementToBeRemoved(() => screen.getByLabelText('Loading'));
    expect(screen.getByText('Citadel of Ricks')).toBeInTheDocument();
  });

  it('should set an edit mode', async () => {
    render(
      <Provider store={store}>
        <Router initialEntries={['/characters/1']}>
          <Routes>
            <Route path="/characters/:id" element={<CharacterPage />} />
          </Routes>
        </Router>
      </Provider>
    );

    server.use(...FETCH_CHARACTER_RESPONSE());

    await waitForElementToBeRemoved(() => screen.getByLabelText('Loading'));
    const button = screen.getByText('Edit');
    fireEvent.click(button);
    expect(button).not.toBeInTheDocument();
    expect(screen.getByText('Cancel')).toBeInTheDocument();
    expect(screen.getByText('Save')).toBeInTheDocument();
  });

  it('should return to ordinary mode from edit', async () => {
    render(
      <Provider store={store}>
        <Router initialEntries={['/characters/1']}>
          <Routes>
            <Route path="/characters/:id" element={<CharacterPage />} />
          </Routes>
        </Router>
      </Provider>
    );

    server.use(...FETCH_CHARACTER_RESPONSE());

    await waitForElementToBeRemoved(() => screen.getByLabelText('Loading'));
    const button = screen.getByText('Edit');
    fireEvent.click(button);
    expect(button).not.toBeInTheDocument();
    expect(screen.getByText('Cancel')).toBeInTheDocument();
    expect(screen.getByText('Save')).toBeInTheDocument();

    const cancelButton = screen.getByText('Cancel');
    fireEvent.click(cancelButton);
    expect(screen.getByText('Edit')).toBeInTheDocument();
  });

  it('should modify the value of character', async () => {
    render(
      <Provider store={store}>
        <Router initialEntries={['/characters/1']}>
          <Routes>
            <Route path="/characters/:id" element={<CharacterPage />} />
          </Routes>
        </Router>
      </Provider>
    );

    server.use(...FETCH_CHARACTER_RESPONSE());

    await waitForElementToBeRemoved(() => screen.getByLabelText('Loading'));
    const button = screen.getByText('Edit');
    fireEvent.click(button);
    expect(button).not.toBeInTheDocument();
    expect(screen.getByText('Cancel')).toBeInTheDocument();
    expect(screen.getByText('Save')).toBeInTheDocument();

    const nameInput = screen.getByPlaceholderText('Name');
    fireEvent.change(nameInput, { target: { value: 'Баку' } });
    fireEvent.click(screen.getByText('Save'));
    await waitForElementToBeRemoved(() => screen.getByText('Save'));
    expect(screen.getAllByText('Баку').length).toBe(3);
  });
});
