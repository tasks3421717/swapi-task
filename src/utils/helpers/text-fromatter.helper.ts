export const formatPrice = (value: number) => {
  return parseInt(value.toString()).toLocaleString();
};
