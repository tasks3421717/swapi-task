export function replacePathParams(path: string, params: { [key: string]: string | number }, prefix = ':') {
  let newPath = path;

  Object.entries(params).forEach(([key, value]) => {
    newPath = newPath.replace(prefix + key, String(value));
  });
  return newPath;
}

export function findNumberValueInString(value: string) {
  return value.replace(/[^0-9]/g, '');
}
