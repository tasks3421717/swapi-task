import { ICharacterDto } from 'store/models/character/dto/fetch-character.dto';

export const searchFilter = (entity: ICharacterDto[], searchExpression: string) =>
  entity.filter((obj: { name: string }) => obj.name.includes(searchExpression));

export const paginationFilter = (entity: ICharacterDto[], limit: number, page: number) =>
  page !== 1 ? entity.slice(limit, page * limit) : entity.slice(0, limit);

export const getTotalPages = (length: number, limit: number) => Math.ceil(length / limit);
