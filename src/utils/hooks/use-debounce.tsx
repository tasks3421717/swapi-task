import { useEffect, useState } from 'react';

function useDebounce<T>(value: T, delay?: number) {
  const [debounceValue, setDebounceVlaue] = useState<T>(value);

  useEffect(() => {
    const timer = setTimeout(() => {
      setDebounceVlaue(value);
    }, delay || 500);

    return () => {
      clearTimeout(timer);
    };
  }, [value, delay]);

  return debounceValue;
}

export default useDebounce;
