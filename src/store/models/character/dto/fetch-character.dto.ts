export interface ICharacterDto {
  id: number;
  name: string;
  species: string;
  status?: string;
  type?: string;
  gender: string;
  url: string;
  created: string;
  imgUrl: string;
  origin: {
    name?: string;
  };
  location: {
    name: string;
    url: string;
  };
  episode: string[];
}
