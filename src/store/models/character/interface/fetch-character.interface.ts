export interface ICharacter {
  id: number;
  name: string;
  species: string;
  status: string;
  type: string;
  gender: string;
  url: string;
  created: string;
  image: string;
  origin: {
    name: string;
  };
  location: {
    name: string;
    url: string;
  };
  episode: string[];
}
