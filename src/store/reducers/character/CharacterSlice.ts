import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { ICharacterDto } from 'store/models/character/dto/fetch-character.dto';
import { fetchCharacter, fetchCharacters } from './CharacterActionCreators';
import { EditFeaturesData } from 'pages/CharacterPage/CharacterDetails/EditFeatures/EditFeatures';

interface CharacterState {
  characters: ICharacterDto[];
  character: ICharacterDto | undefined;
  count: number;
  isLoading: boolean;
  error: string;
}

const initialState: CharacterState = {
  characters: [],
  character: undefined,
  count: 0,
  isLoading: true,
  error: ''
};

export const characterSlice = createSlice({
  name: 'characters',
  initialState,
  reducers: {
    updateCharacter(state, action: PayloadAction<EditFeaturesData>) {
      if (state.character) {
        const updatedcharacter = { ...state.character, ...action.payload };
        state.character = updatedcharacter;
      }
    }
  },
  extraReducers: {
    [fetchCharacters.fulfilled.type]: (
      state,
      action: PayloadAction<{ characters: ICharacterDto[]; count: number }>
    ) => {
      state.isLoading = false;
      state.error = '';
      state.characters = action.payload.characters;
      state.count = action.payload.count;
    },
    [fetchCharacters.pending.type]: (state) => {
      state.isLoading = true;
    },
    [fetchCharacters.rejected.type]: (state, action: PayloadAction<string>) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    [fetchCharacter.fulfilled.type]: (state, action: PayloadAction<{ character: ICharacterDto }>) => {
      state.isLoading = false;
      state.error = '';
      state.character = action.payload.character;
    },
    [fetchCharacter.pending.type]: (state) => {
      state.isLoading = true;
    },
    [fetchCharacter.rejected.type]: (state, action: PayloadAction<string>) => {
      state.isLoading = false;
      state.error = action.payload;
    }
  }
});

export default characterSlice.reducer;
