import axios from 'axios';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { API_URL } from 'config';
import { ICharacter } from 'store/models/character/interface/fetch-character.interface';

export const fetchCharacters = createAsyncThunk(
  'character/fetchAll',
  async ({ page, searchValue }: { page: number; searchValue: string }, thunkApi) => {
    try {
      let params: { page: number; name?: string } = { page };

      if (searchValue) {
        params = { ...params, name: searchValue };
      }

      const api = API_URL + `/character`;
      const response = await axios.get(api, {
        params
      });

      const charactersDto = response.data.results.map((character: ICharacter) => ({
        id: character.id,
        name: character.name,
        species: character.species,
        status: character.status,
        type: character.type,
        gender: character.gender,
        location: character.location,
        origin: character.origin,
        url: character.url,
        created: character.created,
        imgUrl: character.image
      }));

      return { characters: charactersDto, count: response.data.info.count };
    } catch (e) {
      return thunkApi.rejectWithValue('Failed');
    }
  }
);

export const fetchCharacter = createAsyncThunk('character/fetch', async ({ id }: { id: number }, thunkApi) => {
  try {
    const api = API_URL + `/character/${id}`;
    const response = await axios.get(api);

    const character = response.data;
    const charactersDto = {
      id: character.id,
      name: character.name,
      species: character.species,
      status: character.status,
      type: character.type,
      gender: character.gender,
      location: character.location,
      origin: character.origin,
      url: character.url,
      created: character.created,
      imgUrl: character.image
    };

    return { character: charactersDto };
  } catch (e) {
    return thunkApi.rejectWithValue('Failed');
  }
});
