import { BrowserRouter } from 'react-router-dom';
import Router from './routes';
import 'styles/global.scss';
import { Provider } from 'react-redux';
import { setupStore } from 'store/store';

export const store = setupStore();

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Router />
      </BrowserRouter>
    </Provider>
  );
}

export default App;
