import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import { SkeletonTheme } from 'react-loading-skeleton';
import 'react-loading-skeleton/dist/skeleton.css';

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);
root.render(
  <React.StrictMode>
    <SkeletonTheme baseColor="#d9d9d9">
      <App />
    </SkeletonTheme>
  </React.StrictMode>
);
