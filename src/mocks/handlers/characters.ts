import { getMockedFilteredCharacters, mockedcharacters } from 'mocks/domains/character';
import { API_URL } from 'config';
import { rest } from 'msw';

export const FETCH_FILTERED_CHARACTERS_RESPONSE = (searchExpression: string, page: number) => {
  const { count, results } = getMockedFilteredCharacters(searchExpression, page);

  const api = API_URL + `/character`;

  return [
    rest.get(api, (req, res, ctx) => {
      const response = { results, info: { count } };
      return res(ctx.status(200), ctx.json(response));
    })
  ];
};

export const FETCH_WITH_SEARCH_VALUE_FILTERED_CHARACTERS_RESPONSE = (searchExpression: string, page: number) => {
  const { count, results } = getMockedFilteredCharacters(searchExpression, page);

  const api = API_URL + `/character`;

  return [
    rest.get(api, (req, res, ctx) => {
      const searchExpressionParam = req.url.searchParams.get('name');
      const pageParam = req.url.searchParams.get('page');

      if (searchExpressionParam === null || pageParam === null) {
        return res(ctx.status(403), ctx.json({ error: { message: ' One of the params is missing' } }));
      }

      const response = { results, info: { count } };

      return res(ctx.status(200), ctx.json(response));
    })
  ];
};

export const FETCH_WITH_PAGE_VALUE_FILTERED_CHARACTERS_RESPONSE = (searchExpression: string, page: number) => {
  const { count, results } = getMockedFilteredCharacters(searchExpression, page);

  const api = API_URL + `/character`;

  return [
    rest.get(api, (req, res, ctx) => {
      const pageParam = req.url.searchParams.get('page');

      if (pageParam === null) {
        return res(ctx.status(403), ctx.json({ error: { message: ' One of the params is missing' } }));
      }

      const response = { results, info: { count } };

      return res(ctx.status(200), ctx.json(response));
    })
  ];
};

export const FETCH_CHARACTER_RESPONSE = () => {
  const api = API_URL + `/character/`;

  return [
    rest.get(api + ':id', (req, res, ctx) => {
      const { id } = req.params;
      if (id !== '1') {
        return res(ctx.status(403));
      }

      const response = { ...mockedcharacters[0] };

      return res(ctx.status(200), ctx.json(response));
    })
  ];
};
